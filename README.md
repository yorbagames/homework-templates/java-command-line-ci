# java command line ci

Example project to Build and Run a Java 11 command line app in GitLab's CI

## Intended Users
- Anyone that wants to use GitLabCI to build their Java console app & needs a bare minimum example that will "Build" and then "Run" their app
- My kid that is taking java (October 2020) so he doesn't have to "zip up" his homework and email it to the instructor like it's 1999 all over again  

## Porting to other Projects
0. Copy `.gitlab-c.yml` to your project
0. Where it says `Main.java`, change it to the relative path to your application entry point
0. The rest of the files are optional for local testing.

## Editing the Code

Use your favorite editor to modify `Main.java`

## Compiling the code

This will build and run `Main.java`

### MacOS
- Install Docker
- run `./build-test` to compile `Main.java` and have your app run from within the Dockerfile container

### GitLab-CI
- Push to GitLab
- The CI will automatically run thanks to the `.gitlab-ci.yml` file

#### How will I know if it worked?
- GitLab will send an email for each failed build & 1 success email when builds start working again
- On the GitLab project page, look at "CI / CD" > "Pipelines" to view all the build runs 
 
### Windows
- Load it up in your local https://bluej.org java IDE
- TODO: install docker for windows & verify `build-test.bat` works correctly
