@echo off

REM "Test environment to approximate the GitLab-CI run"
REM "(I don't want multiple versions of Java on my dev machine)"

REM "1. Dockerfile compiles the code during image build"
REM "2. Run the docker image to get the output"

docker build -t java-test-app .

echo "Running the app..."
echo "---"
docker run java-test-app
echo "---"
