# Used for local testing
# Avoids cluttering my system will multiple java images

FROM openjdk:11.0.8-jdk
COPY . /var/www/java
WORKDIR /var/www/java
RUN javac Main.java
CMD ["java", "Main"]