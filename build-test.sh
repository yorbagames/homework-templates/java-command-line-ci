#!/usr/bin/env bash

# Test environment to approximate the GitLab-CI run
# (I don't want multiple versions of Java on my dev machine)
#
# 1. Dockerfile compiles the code during image build
# 2. Run the docker image to run the app and get output
#

set -e

IMAGE_NAME='java-test-app'
IMAGE_CLEAN=''

while [[ $# -gt 0 ]]
do
  key="$1"

  case $key in
    -n|--name)
      IMAGE_NAME="$2"
      shift
      shift
      ;;
    -c|--clean)
      IMAGE_CLEAN='--no-cache'
      shift
      ;;
    *)
      shift
      ;;
  esac
done

docker build ${IMAGE_CLEAN} -t "${IMAGE_NAME}" .

echo "Running the app..."
echo "---"
docker run "${IMAGE_NAME}"
echo "---"
